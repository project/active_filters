<?php

declare(strict_types=1);

namespace Drupal\Tests\active_filters\Unit;

/**
 * Tests active filter value objects.
 *
 * @coversDefaultClass \Drupal\active_filters\ActiveFilter\ActiveFilterBase
 *
 * @group active_filters
 */
final class ActiveFilterValueObjectsTest extends ActiveFilterUnitTestBase {

  /**
   * Test getting the label.
   *
   * @covers ::getLabel
   */
  public function testGetLabel(): void {
    self::assertSame($this->label, $this->activeFilter->getLabel());
    self::assertSame($this->label, $this->group->getLabel());
  }

  /**
   * Test getting the name.
   *
   * @covers ::getName
   */
  public function testGetName(): void {
    self::assertSame($this->name, $this->activeFilter->getName());
    self::assertSame($this->name, $this->group->getName());
  }

  /**
   * Test getting the configuration.
   *
   * @covers ::getConfiguration
   */
  public function testGetConfiguration(): void {
    self::assertSame($this->configuration, $this->activeFilter->getConfiguration());
    self::assertSame($this->configuration, $this->group->getConfiguration());
  }

  /**
   * Test getting the filter.
   *
   * @covers ::getFilter
   */
  public function testGetFilter(): void {
    self::assertSame($this->filter, $this->activeFilter->getFilter());
    self::assertSame($this->filter, $this->group->getFilter());
  }

  /**
   * Test getting the view.
   *
   * @covers ::getView
   */
  public function testGetView(): void {
    self::assertSame($this->view, $this->activeFilter->getView());
    self::assertSame($this->view, $this->group->getView());
  }

  /**
   * Test getting the value.
   *
   * @covers \Drupal\active_filters\ActiveFilter\ActiveFilter::getValue
   */
  public function testGetValue(): void {
    self::assertSame($this->value, $this->activeFilter->getValue());
  }

  /**
   * Test checking if the active filter is removable.
   *
   * @covers \Drupal\active_filters\ActiveFilter\ActiveFilter::isRemovable
   */
  public function testIsRemovable(): void {
    self::assertSame($this->removable, $this->activeFilter->isRemovable());
  }

  /**
   * Test getting the grouped active filters.
   *
   * @covers \Drupal\active_filters\ActiveFilter\ActiveFilterGroup::getActiveFilters
   */
  public function testGetActiveFilters(): void {
    self::assertSame($this->activeFilters, $this->group->getActiveFilters());
  }

}
